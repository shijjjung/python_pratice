import config
import pymysql
#pip3 install pymysql
class SqlConnect():
    def __init__(self):
        self.host = config.host
        self.user = config.user
        self.passwd = config.password
        self.dbName = config.dbName
        self.port = config.port
        self.charset = config.charset
        self.conn = None
        self.cursor = None
        self.main() 
    def __del__(self):
        if not self.conn:
            return
        self.cursor.close()
        self.conn.close()

    def main(self):
        if not self.doMySqlConnect():
            return
    # 資料庫連線
    def connectDb(self, dbName):
        try:
            mysqldb = pymysql.connect(
                    host=self.host,
                    port=self.port,
                    user=self.user,
                    passwd=self.passwd,
                    charset=self.charset,
                    database=self.dbName)
            return mysqldb
        except Exception as e:
            print(e)
        return None
    def doMySqlConnect(self):
        try:
            #連結資料庫
            self.conn = pymysql.connect(
                host=self.host,
                port=self.port,
                user=self.user,
                passwd=self.passwd,
                charset=self.charset)
            self.cursor = self.conn.cursor()#宣告執行命令的指標
            if not self.isDbExist(self.dbName):
                self.createNewDataBase(self.dbName)
            self.cursor.close()
            self.conn.close()
            self.conn = self.connectDb(self.dbName)
            return True
        except pymysql.err.OperationalError:
            return False
    # 資料表是否存在
    # @param string tbName 
    # @return bool
    def isTableExist(self, tbName):
        return self.cursor.execute(f"SHOW TABLES LIKE '%{tbName}%';") > 0
    # 資料庫是否存在
    # @param string dbName 
    # @return bool
    def isDbExist(self, dbName):
        return self.cursor.execute(f"SHOW DATABASES LIKE '%{dbName}%';") > 0
    # 創建新資料庫
    # @param string dbName 
    def createNewDataBase(self, dbName):
        sql = "CREATE DATABASE %s;"%dbName
        try:
            self.cursor.execute(sql)
        except:
            print("新增資料庫失敗")
    # 創建新資料表
    # @param string tableName 
    def createNewTable(self, tableName):
        paramId = "id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY"
        paramdate = "date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP"
        paramTitle = "title VARCHAR(255)"#title VARCHAR(255) NOT NULL
        sql = "CREATE TABLE IF NOT EXISTS %s (%s, %s);"%tableName, paramId, paramdate
        # sql = "CREATE TABLE %s (%s, %s);"%tableName, paramId, paramdate
        self.cursor.execute(sql)
    # 新增一筆資料
    # @param table param
    # param 欄位格式 {'title':"'第一筆資料'", 'message': "'內容'"}
    # self.addOneData(t)
    def addOneData(self, tableName, param):
        if not self.isTableExist(tableName):
            print("未建立資料表%s"%tableName)
            return "未建立資料表%s"%tableName
        tableName = param.tbName
        title = param.title or "tmpTitle"
        names = ''
        values = ''
        for k, v in _dict.items():
            names+=k + ","
            values+=v + ","
        #[:-1] 去掉最後一個逗號
        sql="INSERT INTO %s (%s)VALUES (%s);"%tableName, names[:-1], values[:-1]
        self.cursor.execute(sql)
    # sql 欄位格式 {'title':"'第一筆資料'", 'message': "'內容'"}
    def featDataByID(self, tableName, _dict, id):
        s = ''
        for k, v in _dict.items():
            s+=k + "=" + v+","
        sql = "UPDATE %s SET %s WHERE CustomerID = %d;"% tableName, s[:-1], id
        self.cursor.execute(sql)
    # >> d.items()
    # [('a', 'Arthur'), ('b', 'Belling')]

    # >> d.keys()
    # ['a', 'b']

    # >> d.values()
    # ['Arthur', 'Belling']
sql = SqlConnect()