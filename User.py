from ILogin import ILogin
from ILogout import ILogout
from interface import implements

class User(implements(ILogin, ILogout)):
    def __init__(self):
        super().__init__()
        self.account = None

    def login(self):
        print("on user Login")

    def logout(self):
        print("on user Logout")

    def setAccount(self, arg1):
        self.account = arg1

    def getAccount(self):
        return self.account

user = User()
user.login()
user.logout()

        